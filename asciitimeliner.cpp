//    Copyright (C) 2013, 2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>

#include "asciitimeliner.hpp"

namespace etl {

    AsciiTimeliner::AsciiTimeliner(int slots) :
        Timeliner(slots) {
    }

    AsciiTimeliner::~AsciiTimeliner() {
    }

    void AsciiTimeliner::display() {
        // top line marker and date
        if (startMarker.size() > 0)
            std::cout << "  " << startMarker;
        if (title.size() > 0)
            std::cout << "  [" << title << "]";

        std::cout << std::endl;

        for (std::size_t i=0; i < allEvents.size(); ++i) {
            
            // left side markers
            if (allEvents[i].marker.size() > 0) {
                std::cout << " " << allEvents[i].marker;
            }
            else {
                std::cout << "     ";
            }

            // actual events
            int nevents = allEvents[i].events.size();
            if (nevents > 0) {
                unsigned int eachWidth = 10000;  // calculate later
                std::cout << " +-- ";
                for (int j=0; j < nevents; ++j) {
                    std::cout << eventStr(allEvents[i].events[j], eachWidth, j);
                }
                std::cout << std::endl;
            }
            else {
                std::cout << " |" << std::endl;
            }
        }

        // bottom line date
        if (endMarker.size() > 0)
            std::cout << "  " << endMarker << std::endl;
    }

    int AsciiTimeliner::getDisplayWidth() const {
        return 80;
    }

    std::string AsciiTimeliner::eventStr(const std::string& str, int maxwidth, int pos) const {
        std::stringstream ss;

        if (pos)
            ss << "][";
        ss << str;

        return ss.str();
    }
}
