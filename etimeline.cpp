//    Copyright (C) 2013, 2014, 2017 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include <memory>
#include <fstream>
#include <iostream>
#include <getopt.h>
#include <stdlib.h>

#include "etlinputexception.hpp"
#include "timeline.hpp"
#include "asciitimeliner.hpp"

#define SLOTS_FOR_DISPLAY   24

void usage() {
    std::cerr << "Usage: etimeline [options] [input_file]" << std::endl;
    std::cerr << "    [-s|--slots <num>]" << std::endl;
    std::cerr << "    [-d|--display-dates]" << std::endl;
    std::cerr << "    [-t|--title string]" << std::endl;
}

static int   numSlots = SLOTS_FOR_DISPLAY;
static bool  displayDates = false;
static char* inputFile;
static std::string title("Timeline of Life");

bool process_options(int argc, char** argv) {
    int c;

    while (1) {
        static struct option long_options[] = {
            {"slots", 1, 0, 's'},
            {"display-dates", 0, 0, 'd'},
            {"title", 1, 0, 't'},
            {0, 0, 0, 0}
        };

        c = getopt_long(argc, argv, "s:t:d", long_options, 0);
        if (c == -1)
            break;

        switch (c) {
        case 'd':
            displayDates = true;
            break;

        case 's':
            numSlots = atoi(optarg);
            break;

        case 't':
            title = std::string(optarg);
            break;

        case '?':
        default:
            usage();
            return(false);
        }
    }

    switch (argc-optind) {
    case 0:
        // use stdin
        break;
    case 1:
        inputFile = argv[optind];
        break;
    default:
        usage();
        return(false);
        break;
    }

    return true;
}

int main(int argc, char** argv) {

    if (! process_options(argc, argv)) {
        return 1;
    }

    std::unique_ptr<etl::Timeline> timeline;
    std::ifstream input;

    try {
        if (inputFile) {
            input.open(inputFile);
            if (! input.is_open() ) {
                std::cerr << "unable to open " << inputFile << std::endl;
                return 1;
            }
            timeline = std::unique_ptr<etl::Timeline>(new etl::Timeline(input, displayDates));
        }
        else {
            timeline = std::unique_ptr<etl::Timeline>(new etl::Timeline(std::cin, displayDates));
        }
    }
    catch (const etl::EtlInputException& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    etl::AsciiTimeliner printer(numSlots);
    printer.addTitle(title);
    timeline->draw(printer);

    return 0;
}
