//    Copyright (C) 2013 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include "timeliner.hpp"

namespace etl {

    Timeliner::Timeliner(int slots) :
        title(),
        allEvents(slots+1) {
    }

    Timeliner::~Timeliner() {
    }

    void Timeliner::addStartMarker(std::string stmk) {
        startMarker = stmk;
    }

    void Timeliner::addEndMarker(std::string endmk) {
        endMarker = endmk;
    }

    void Timeliner::addTitle (std::string title) {
        this->title = title;
    }

    void Timeliner::addEvent(std::string eventTitle, int slot) {
        allEvents[slot].events.push_back(eventTitle);
    }

    void Timeliner::addMarker(std::string marker, int slot) {
        allEvents[slot].marker = marker;
    }

    int Timeliner::numSlots() {
        return allEvents.size() - 1;
    }

    void Timeliner::display() {
    }

}
