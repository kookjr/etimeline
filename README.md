[![pipeline status](https://gitlab.com/kookjr/etimeline/badges/master/pipeline.svg)](https://gitlab.com/kookjr/etimeline/commits/master)


# Easy Timeline -- a C++ program to display events in a timeline format.

Easy Timeline is used to show the relative time between events. The
event input format is a simple text file that can be created with any
text editor.

Easy Timeline contains the following features:

* Easy event input format

* Automatically labels timeline with months or years

* Uses a simple ASCII format to display timelines

* Can be extended to use other display systems by subclassing Timeliner

The input format for events is a text file. Each line has one
event. Blank lines and comments are not allowed. The line format is

```
  YYYY-MM-DD event description
```

Easy Timeline depends on a few boost C++ features; regex, date_time. It also
uses CMake.

## Installation

Easy Timeline must be built from source code. It requires at least the
above mentioned Boost libraries to be installed. Then

```
  mkdir build && cd build && cmake .. && make
```

Note that using this cmake command line option will perform static
analysis on the code in addition to the build using clang-tidy.

```
  cmake -DDO_STATIC_ANALYSIS=ON ..
```

## Sample Usage

Display a timeline for one of the sample input files.

```
  ./etimeline testinput_mon
```

## Development
### Source Repository

Easy Timeline is currently hosted at gitlab. The gitlab web page is
https://gitlab.com/kookjr/etimeline. The public git clone URL is
 
  * https://gitlab.com/kookjr/etimeline.git

### Running the Test Suite

To test the automatic selection of month labels

```
  ./etimeline testinput_mon
```

To test the automatic selection of year labels

```
  ./etimeline testinput_year
```

Other test inputs are

```
  ./etimeline testinput_mon
  ./etimeline testinput_comments
```

### Issues and Bug Reports

Report and follow issue status at the project's GitLab site, on the Issues tab.

## License

GNU General Public License version 3. For details,
see file "COPYING".
