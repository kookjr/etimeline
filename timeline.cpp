//    Copyright (C) 2013, 2014, 2022 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include <sstream>

#include <assert.h>

#include "boost/date_time/gregorian/gregorian.hpp"

#include "timeline.hpp"

namespace etl {

    Timeline::Timeline(std::istream& instr, bool datePrefix) :
        events(),
        showDate(datePrefix) {

        std::string line;

        // should throw on I/O err
        while (getline(instr, line)) {
            if (! (line.size() == 0 || line[0] == '#')) {
                events.insert(Event(line));
            }
        }

        if (instr.bad())
            throw EtlInputException(std::string("I/O error"));
    }

    Timeline::~Timeline() {
    }

    void Timeline::draw(Timeliner& tlr) {
        long tot_slots = tlr.numSlots();

        std::set<Event>::iterator first, cur;
        std::set<Event>::reverse_iterator last;
        first = events.begin();
        last  = events.rbegin();

        assert(first != events.end());
        assert(last !=  events.rend());

        // if each output line has a date (-d option), including the
        // first and last line, do not show the top and bottom line
        // dates as they are redundant
        if (! showDate) {
            tlr.addStartMarker(boost::gregorian::to_simple_string((*first).getDate()));
            tlr.addEndMarker(boost::gregorian::to_simple_string((*last).getDate()));
        }

        double total_days = (*first).daysBetween(*last);

        // add events from timeline
        for (cur=first; cur != events.end(); ++cur) {
            double days = (*first).daysBetween(*cur);
            double slot = (days/total_days) * tot_slots;
            std::string datestr("");
            if (showDate)
                datestr = boost::gregorian::to_simple_string((*cur).getDate()) + " ";
            tlr.addEvent(datestr + (*cur).getDescription(), (int )slot);
        }

        boost::gregorian::date d = (*first).getDate();
        if (total_days < 365) {
            // add labels from timeline - months
            d += boost::gregorian::months(1);
            d = boost::gregorian::date(d.year(), d.month(), 1);
            while (d < (*last).getDate()) {
                double days = (d - (*first).getDate()).days();
                double slot = (days/total_days) * tot_slots;
                std::stringstream ss;
                ss << " " << d.month().as_short_string();
                tlr.addMarker(ss.str(), (int )slot);
                d += boost::gregorian::months(1);
            }
        }
        else {
            // add labels from timeline - years
            d += boost::gregorian::years(1);
            d = boost::gregorian::date(d.year(), d.month(), 1);
            while (d < (*last).getDate()) {
                double days = (d - (*first).getDate()).days();
                double slot = (days/total_days) * tot_slots;
                std::stringstream ss;
                ss << d.year();
                tlr.addMarker(ss.str(), (int )slot);
                d += boost::gregorian::years(1);
            }
        }
        tlr.display();
    }

}
