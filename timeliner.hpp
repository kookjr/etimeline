#ifndef TIMELINER_H
#define TIMELINER_H

//    Copyright (C) 2013 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include <vector>
#include <string>

namespace etl {

    class Timeliner {
    public:
        Timeliner(int slots);
        ~Timeliner();

        void addTitle(std::string title);
        void addStartMarker(std::string stmk);
        void addEndMarker(std::string endmk);

        void addEvent (std::string eventTitle, int slot);
        void addMarker(std::string marker,     int slot);

        virtual void display();

        int numSlots();

    protected:
        struct TlData {
            TlData() : events(), marker() { }
            std::vector<std::string> events;
            std::string marker;
        };

        std::string title;
        std::string startMarker;
        std::string endMarker;
        std::vector<TlData> allEvents;

    private:
    };

}

#endif //TIMELINER_H
