#ifndef ASCIITIMELINER_H
#define ASCIITIMELINER_H

//    Copyright (C) 2013 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include "timeliner.hpp"

namespace etl {

    class AsciiTimeliner : public Timeliner {
    public:
        AsciiTimeliner(int slots);
        ~AsciiTimeliner();

        virtual void display();

    private:
        int getDisplayWidth() const;
        std::string eventStr(const std::string& str, int maxwidth, int pos) const;
    };
}

#endif //ASCIITIMELINER_H
