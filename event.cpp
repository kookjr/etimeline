//    Copyright (C) 2013 Mathew Cucuzella, kookjr@gmail.com
//
//    This file is part of Easy Timeline; a program to display
//    events in a timeline format.
//
//    Easy Timeline is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Easy Timeline is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Easy Timeline.  If not, see <http://www.gnu.org/licenses/>.

#include "boost/regex.hpp"

#include "etlinputexception.hpp"
#include "event.hpp"

namespace etl {

    Event::Event() :
        description(),
        point( boost::gregorian::day_clock::local_day() ) {
    }

    Event::Event(const std::string& evLine) :
        description(),
        point() {

        boost::smatch what;
        const boost::regex input_regex( "(\\d{4}-\\d{2}-\\d{2}) +(.*)" );

        if (! boost::regex_match(evLine, what, input_regex))
            throw EtlInputException(std::string("invalid event line: " + evLine));

        point = boost::gregorian::date(boost::gregorian::from_simple_string(what[1]));
        description = what[2];
    }

    Event::~Event() {
    }

    std::string Event::getDescription() const {
        return description;
    }

    boost::gregorian::date Event::getDate() const {
        return point;
    }

    long Event::daysBetween(const Event& ev) const {
        return (ev.point - point).days();
    }

    bool Event::operator<(const Event& ev) const {
        return point < ev.point;
    }

}
